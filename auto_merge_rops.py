MERGE_NAME = "AUTO_MERGE_ROPS"

def get_rops():
    rop_geo = hou.nodeType(hou.sopNodeTypeCategory(), "rop_geometry")
    rop_abc = hou.nodeType(hou.sopNodeTypeCategory(), "rop_alembic")
    rops_geo = rop_geo.instances()
    rops_abc = rop_abc.instances()
    rops_total = rops_geo + rops_abc
    return rops_total
    
def check_if_fetch_exists(path):
    rop_fetch_type = hou.nodeType(hou.ropNodeTypeCategory(), "fetch")
    fetch_nodes = rop_fetch_type.instances()
    for node in fetch_nodes:
        if node.evalParm("source") == path:
            return True
    return False
    
    
def create_fetch_rops(rops_total):
    if not rops_total:
        return None
    new_nodes = []
    for node in rops_total:
        path = node.path()
        if check_if_fetch_exists(path):
            continue
        new_fetch = hou.node("/out").createNode("fetch", node.name())
        new_fetch.parm("source").set(path)
        new_fetch.moveToGoodPosition()
        new_nodes.append(new_fetch)
    return new_nodes
            
def create_merge_node():
    merge_node = hou.node("/out/{0}".format(MERGE_NAME))
    if merge_node:
        return merge_node
    merge_node = hou.node("/out").createNode("merge", MERGE_NAME)
    merge_node.moveToGoodPosition()
    return merge_node

def merge_all_nodes(merge_node, new_nodes):
    if not new_nodes:
        return
    for node in new_nodes:
        merge_node.setNextInput(node)
    merge_node.moveToGoodPosition()
        
rops_total = get_rops()
new_nodes = create_fetch_rops(rops_total)
merge_node = create_merge_node()
merge_all_nodes(merge_node, new_nodes) 

